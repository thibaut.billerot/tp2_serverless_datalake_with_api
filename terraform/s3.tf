# Créer un bucket aws_s3_bucket
resource "aws_s3_bucket" "bucket" {
  bucket = "s3-job-offer-billerot-thibaut"
  acl    = "private"
  force_destroy = true

  tags = {
    Name        = "TP2 Serverless"
    Environment = "Dev"
  }
}

# Créer un répertoire aws_s3_bucket_object avec job_offers/raw/
resource "aws_s3_bucket_object" "object" {
  bucket = "${aws_s3_bucket.bucket.bucket}"
  key    = "job_offers/raw/"
  source = "/dev/null"

  # The filemd5() function is available in Terraform 0.11.12 and later
  # For Terraform 0.11.11 and earlier, use the md5() function and the file() function:
  # etag = "${md5(file("path/to/file"))}"
  # etag = "${filemd5("path/to/file")}"
}


# Créer un event aws_s3_bucket_notification pour trigger la lambda
resource "aws_s3_bucket_notification" "bucket_notification" {
  bucket = "${aws_s3_bucket.bucket.id}"

  lambda_function {
    lambda_function_arn = "${aws_lambda_function.test_lambda.arn}"
    events              = ["s3:ObjectCreated:*"]
    filter_prefix       = "job_offers/raw/"
    filter_suffix       = ".csv"
  }
}
